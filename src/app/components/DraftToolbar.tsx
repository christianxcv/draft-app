import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FC, useState } from 'react';

import { PickState } from '../../store/slices/draftArenaSlice';

type DraftToolbarProps = {
  state: PickState
  viewIcon: string
  changeRanksView: () => void
  undoPick: () => void
  draftKicker: () => void
  draftDefense: () => void
}

export const DraftToolbar: FC<DraftToolbarProps> = ({
  state,
  changeRanksView,
  viewIcon,
  undoPick,
  draftDefense,
  draftKicker,
}) => {
  const {
    overall,
    currRound,
    roundPick
  } = state
  const [dropdownClosed, setDropdownClosed] = useState(true)

  return (
    <div className="DraftToolbar">
      <div className="numbers" onClick={() => setDropdownClosed(!dropdownClosed)}>
        <div className="inner">
          <p><span>Overall </span>{overall}</p>
          <p><span>Round </span>{currRound}</p>
          <p><span>Pick </span>{roundPick}</p>
        </div>
        <div className={`buttons ${dropdownClosed ? 'closed' : ''}`}>
          <button className="defense" onClick={draftDefense}>Draft Defense</button>
          <button className="kicker" onClick={draftKicker}>Draft Kicker</button>
          {
            overall > 1 &&
            <button className="undo-button" onClick={undoPick}>Undo Pick</button>
          }
        </div>
      </div>
      <div className="icon-container" onClick={changeRanksView}>
        <FontAwesomeIcon icon="chevron-down" className={`icon ${viewIcon}`} />
      </div>
    </div>
  );
}