import { FC } from 'react';
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd';

import { PlayerV2 } from '../../../models/player';
import { PlayerRow } from '../ranks/PlayerRow';

type PlayerQueueProps = {
  queue: PlayerV2[]
  draftPlayer?: (playerId: number) => void
  queuePlayer?: (playerId: number) => void
  dequeuePlayer?: (playerId: number) => void
  updateQueueOrder?: (oldIndex: number, newIndex: number) => void
}

export const PlayerQueue: FC<PlayerQueueProps> = ({
  queue,
  draftPlayer,
  queuePlayer,
  dequeuePlayer,
  updateQueueOrder
}) => {
  const onDragEnd = (result: DropResult) => {
    if (!result.destination || !updateQueueOrder)
      return

    updateQueueOrder(
      result.source.index,
      result.destination.index
    )
  }

  return (
    <div className="Group PlayerQueue">
      <h3>Queue {queue.length ? `(${queue.length})` : ''}</h3>
      {!queue.length ? (
        <div className="empty-queue">
          <h4>Add players to queue</h4>
        </div>
      ) : (
        <DragDropContext onDragEnd={onDragEnd}>
          <Droppable droppableId="queue">
            {provided => (
              <div className="populated-queue"
                {...provided.droppableProps}
                ref={provided.innerRef}>
                {
                  queue.map((player, index) => (
                    <Draggable key={player.playerId} draggableId={player.playerId.toString()} index={index}>
                      {provided => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}>
                          <PlayerRow
                            key={player.playerId}
                            player={player}
                            draftPlayer={draftPlayer}
                            queuePlayer={queuePlayer}
                            dequeuePlayer={dequeuePlayer}
                          />
                        </div>
                      )
                      }
                    </Draggable>
                  ))
                }
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      )}
    </div>
  )
}