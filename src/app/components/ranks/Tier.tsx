import { FC } from 'react';

import { PlayerPosition, PopulatedTier } from '../../../models/player';
import { PlayerRow } from './PlayerRow';

type TierProps = {
  tier: PopulatedTier
  isUserPick?: boolean
  playerPosition: PlayerPosition
  draftPlayer?: (playerId: number) => void
  queuePlayer?: (playerId: number) => void
  dequeuePlayer?: (playerId: number) => void
  selectPlayer?: (playerId: number) => void
  hideDraftedPlayers?: boolean
  provided?: any
}

export const Tier: FC<TierProps> = ({
  tier,
  isUserPick,
  draftPlayer,
  hideDraftedPlayers,
  queuePlayer,
  dequeuePlayer,
  selectPlayer,
}) => {

  const listPlayers = () => {
    return tier.players.map((player) => {
      return (
        <PlayerRow
          key={player.playerId}
          player={player}
          draftPlayer={draftPlayer}
          hideDraftedPlayers={hideDraftedPlayers}
          queuePlayer={queuePlayer}
          dequeuePlayer={dequeuePlayer}
          onPlayerClick={selectPlayer}
          isUserPick={isUserPick}
        />
      )
    })
  }

  const allPlayersDrafted = () => tier.players.every((player) => player.drafted)

  return (
    <div className={hideDraftedPlayers && allPlayersDrafted() ? 'hide-tier' : ''}>
      <div className="tier-header">
        <div className="tier-number">
          <span>{tier.playerPosition} - Tier {tier.tierNumber}</span>
        </div>
        <hr />
      </div>
      {listPlayers()}
    </div>
  )
}