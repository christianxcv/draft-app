import { FC, MouseEvent } from 'react'
import styled from 'styled-components'
import { DARK_MODE_CTA, DARK_MODE_DRAWER_ROW, DARK_MODE_TEXT_PRIMARY, DARK_MODE_TEXT_SECONDARY, POSITION_INDICATOR_COLOR } from '../../../../../styles/variables'
import { PlayerV2, PopulatedTier, UserFlag } from '../../../../../models/player'
import { TbPlaylistAdd, TbPlaylistX } from 'react-icons/tb'
import { BiTargetLock } from 'react-icons/bi'
import { TbEyeCheck } from 'react-icons/tb'
import { BsFlagFill } from 'react-icons/bs'

type TierTableProps = {
  tier: PopulatedTier
  isUserPick: boolean
  displayTierLabels: boolean
  searchQuery: string
  hideDraftButton?: boolean
  draftPlayer: (playerId: number) => void
  queuePlayer: (playerId: number) => void
  dequeuePlayer: (playerId: number) => void
  selectPlayer: (playerId: number) => void
}

const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
`

const TableRow = styled.tr`
  background: ${DARK_MODE_DRAWER_ROW};
  height: 40px;
  border-bottom: 2px solid #28293b;
  cursor: pointer;
`

const TableRowCell = styled.td<{ $isLeftAlign?: boolean }>`
  color: ${DARK_MODE_TEXT_PRIMARY};
  vertical-align: middle;
  text-align: ${ props => props.$isLeftAlign ? 'left' : 'right' };
  font-size: .9rem;


  &:first-of-type {
    padding-left: 14px;
  }

  &:last-of-type {
    padding-right: 18px;
  }
`

const DraftButton = styled.button<{ $isUserPick: boolean }>`
  border: 1px solid #4B59FB;
  background: ${ props => props.$isUserPick ? '#4B59FB' : 'transparent' };
  color: ${DARK_MODE_TEXT_PRIMARY};
  width: 60px;
  height: 30px;
  font-size: .9rem;
`

const TierLabel = styled.div`
  display: flex;
  height: 16px;
  padding: 0 16px;
  background: #545677;
  color: ${DARK_MODE_TEXT_PRIMARY};
  font-size: .7rem;
  margin: 2px 0 3px;
  border-radius: 3px;
  align-items: center;
  font-weight: 700;
`

export const TierTable: FC<TierTableProps> = ({ 
  tier,
  isUserPick,
  displayTierLabels,
  hideDraftButton,
  searchQuery,
  draftPlayer,
  queuePlayer,
  dequeuePlayer,
  selectPlayer,
}: TierTableProps) => {
  const handleQueueButtonClick = (e: MouseEvent<HTMLTableCellElement>, player: PlayerV2) => {
    e.stopPropagation()

    if (player.queued) {
      dequeuePlayer(player.playerId)
    } else {
      queuePlayer(player.playerId)
    }
  }

  const handleDraftButtonClick = (e: MouseEvent<HTMLTableCellElement>, playerId: number) => {
    e.stopPropagation()
    draftPlayer(playerId)
  }

  const renderUserFlagIcon = (userFlag?: UserFlag) => {
    switch (userFlag) {
      case UserFlag.AllIn:
        return <BiTargetLock color="#02d402" size="1.2rem" />
      case UserFlag.AllOut:
        return <BsFlagFill color="#eb0000" size="1rem" />
      case UserFlag.Neutral:
        return <TbEyeCheck color="#fffc00" size="1.2rem" />
      case UserFlag.Unranked:
      default:
        return null
    }
  }

  return (
    <>
      {
        displayTierLabels &&
          <TierLabel>Tier {tier.tierNumber}</TierLabel>
      }
      <Table>
        <tbody>
          {
            tier.players.filter(player => !player.drafted && player.name.toLowerCase().indexOf(searchQuery) !== -1).map(player => (
              <TableRow key={player.playerId} onClick={() => selectPlayer(player.playerId)}>
                <TableRowCell style={{ width: '5%', textAlign: 'left' }} onClick={(e: MouseEvent<HTMLTableCellElement>) => handleQueueButtonClick(e, player)}>
                  <div style={{ background: player.queued ? DARK_MODE_CTA : 'transparent', borderRadius: '20px', textAlign: 'center', height: '28px', width: '28px', cursor: 'pointer' }}>
                  {
                    player.queued ? (
                        <TbPlaylistX style={{ height: '26px', width: '26px' }} />
                    ) : (
                      <TbPlaylistAdd style={{ height: '26px', width: '26px' }} />
                    )
                  }
                  </div>
                </TableRowCell>
                {
                  !hideDraftButton ? (
                    <TableRowCell style={{ width: '7%' }} $isLeftAlign
                    onClick={(e: MouseEvent<HTMLTableCellElement>) => handleDraftButtonClick(e, player.playerId)}>
                      <DraftButton 
                        $isUserPick={isUserPick}
                        >
                          { isUserPick ? 'Draft' : 'Mark' }
                      </DraftButton>
                    </TableRowCell>
                  ) : null
                }
                <TableRowCell style={{ width: '4%' }} $isLeftAlign>{player.position}{player.rank}</TableRowCell>
                <TableRowCell style={{ width: '2%' }}>{tier.tierNumber}</TableRowCell>
                <TableRowCell style={{ width: '4%' }}>{renderUserFlagIcon(player.userFlag)}</TableRowCell>
                <TableRowCell style={{ width: '3%' }}>
                  <div style={{ height: '8px', width: '8px', borderRadius: '5px', background: POSITION_INDICATOR_COLOR[player.position as keyof typeof POSITION_INDICATOR_COLOR], float: 'right', marginRight: '5px' }}></div>
                </TableRowCell>
                <TableRowCell $isLeftAlign>
                  <div style={{ fontWeight: 500 }}>{player.name}</div>
                  <div style={{ fontSize: '.6rem', color: DARK_MODE_TEXT_SECONDARY }}>{player.position} - {player.team}</div>
                </TableRowCell>
                <TableRowCell style={{ width: '7%' }}>{player.averageDraftPosition?.adp || '-'}</TableRowCell>
                <TableRowCell style={{ width: '7%' }}>{player.averageDraftPosition?.positionRank || '-'}</TableRowCell>
                <TableRowCell style={{ width: '5%' }}>{player.byeWeek}</TableRowCell>
                <TableRowCell style={{ width: '5%' }}>{player.sos}</TableRowCell>
                <TableRowCell style={{ width: '8%' }}>{player.projections?.fPts || '-'}</TableRowCell>
                <TableRowCell style={{ width: '7%' }}>{player.projections?.ppg || '-'}</TableRowCell>
                <TableRowCell style={{ width: '7%' }}>{player.previousYear?.positionRank || '-'}</TableRowCell>
                <TableRowCell style={{ width: '8%' }}>{player.previousYear?.fPts || '-'}</TableRowCell>
                <TableRowCell style={{ width: '7%' }}>{player.previousYear?.ppg || '-'}</TableRowCell>
              </TableRow>
            ))
          }
        </tbody>
      </Table>
    </>
  )
}