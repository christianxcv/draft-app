import { FC } from 'react';

import { PlayerPosition, PopulatedTier } from '../../../models/player';
import { Tier } from './Tier';

type PositionRanksProps = {
  position: PlayerPosition
  ranks: PopulatedTier[]
  isUserPick?: boolean
  draftPlayer?: (playerId: number) => void
  queuePlayer?: (playerId: number) => void
  dequeuePlayer?: (playerId: number) => void
  selectPlayer?: (playerId: number) => void
  hideDraftedPlayers?: boolean
}

export const PositionRanks: FC<PositionRanksProps> = ({
  position,
  ranks,
  isUserPick,
  draftPlayer,
  hideDraftedPlayers,
  queuePlayer,
  dequeuePlayer,
  selectPlayer,
}) => {
  const createTiers = () =>
    ranks.map(tier => (

      <Tier
        key={tier.tierNumber}
        draftPlayer={draftPlayer}
        tier={tier}
        playerPosition={position}
        hideDraftedPlayers={hideDraftedPlayers}
        queuePlayer={queuePlayer}
        dequeuePlayer={dequeuePlayer}
        selectPlayer={selectPlayer}
        isUserPick={isUserPick}
      />
    )
    )

  return (
    <div className="Group">
      {createTiers()}
    </div>
  )
}