import { FC } from 'react';

import { AllTiers, PlayerPosition, PlayerV2 } from '../../../models/player';
import { PlayerQueue } from '../queue/PlayerQueue';
import { PositionRanks } from './PositionRanks';

const positionSortOrder: Record<PlayerPosition, number> = {
  [PlayerPosition.QB]: 1,
  [PlayerPosition.RB]: 2,
  [PlayerPosition.WR]: 3,
  [PlayerPosition.TE]: 4,
  [PlayerPosition.OVR]: 5
}

type RankingsProps = {
  rankings: AllTiers
  playerQueue?: PlayerV2[]
  isUserPick?: boolean
  draftPlayer?: (playerId: number) => void
  queuePlayer?: (playerId: number) => void
  dequeuePlayer?: (playerId: number) => void
  selectPlayer?: (playerId: number) => void
  updateQueueOrder?: (oldIndex: number, newIndex: number) => void
}

export const Rankings: FC<RankingsProps> = ({
  rankings,
  playerQueue,
  isUserPick,
  draftPlayer,
  queuePlayer,
  dequeuePlayer,
  selectPlayer,
  updateQueueOrder
}) => {

  const renderGroups = () => {
    if (!rankings) return
    return (<div></div>)
    // return Object.keys(rankings)
    //   .sort((a, b) => positionSortOrder[a as PlayerPosition] - positionSortOrder[b as PlayerPosition])
    //   .map(position => rankings[position as PlayerPosition])
    //   .map((positionRanks) => {
    //     if (!positionRanks || !positionRanks.length) return null
    //     return (
    //       <PositionRanks
    //         key={positionRanks[0].playerPosition}
    //         ranks={positionRanks}
    //         position={PlayerPosition.OVR} // Deprecated
    //         draftPlayer={draftPlayer}
    //         queuePlayer={queuePlayer}
    //         dequeuePlayer={dequeuePlayer}
    //         selectPlayer={selectPlayer}
    //         isUserPick={isUserPick}
    //         hideDraftedPlayers
    //       />
    //     )
    //   })
  }

  return (
    <div className="Rankings">
      <div className="ranks">
        <div className="groups">
          {renderGroups()}
          {
            playerQueue && !!playerQueue.length &&
            <PlayerQueue
              queue={playerQueue}
              draftPlayer={draftPlayer}
              queuePlayer={queuePlayer}
              dequeuePlayer={dequeuePlayer}
              updateQueueOrder={updateQueueOrder}
            />
          }
        </div>
      </div>
    </div>
  )
}