import { DraftConfig, DraftOrder, DraftOrigin, DraftPicks, DraftsPage } from '../../models/draft';

export type GetDraftResponse = {
  picks: DraftPicks
  config: DraftConfig
  draftOrder: DraftOrder
  draftOrigin: DraftOrigin
}

export type GetDraftsResponse = DraftsPage


export type CreateDraftResponse = {
  draftId: string
}

export type DraftPlayerRequest = {
  playerId: number
  pickNumber: number
}
