import { PlayersByPosition } from '../../models/player';

export type GetPlayersResponse = {
  players: PlayersByPosition
}

export type GetPlayerDetailResponse = {
  notes: string
}