import { RanksByPosition } from '../../models/ranks';

export type GetRanksResponse = {
  ranks: RanksByPosition
}