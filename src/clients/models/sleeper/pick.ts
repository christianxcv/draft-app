export type SleeperPick = {
  round: number
  roster_id: string | null
  player_id: string
  picked_by: string // user id
  pick_no: number
  metadata: SleeperPickMetadata
  is_keeper: boolean | null
  draft_slot: number
  draft_id: string
}

export type SleeperPickMetadata = {
  years_exp: string // ex: "3"
  team: string,
  status: string // ex: "Active"
  sport: string // "nfl"
  position: string
  player_id: string
  number: string // ex: "18"
  news_updated: string // epoch time
  last_name: string
  injury_status: string
  first_name: string
}