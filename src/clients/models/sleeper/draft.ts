export type SleeperDraft =   {
  type: string // snake
  status: string // drafting, complete
  start_time: number // epoch time
  sport: string // nfl
  settings: SleeperDraftSettings
  season_type: string // regular
  season: string // "2023"
  metadata: SleeperDraftMetadata,
  league_id?: string
  last_picked: number // epoch time
  last_message_time: number // epoch time
  last_message_id: string
  draft_order: SleeperDraftOrder
  draft_id: string
  creators: string[] // array of user ids
  created: number // epoch time
}

export type SleeperDraftSettings = {
  teams: number
  slots_wr: number
  slots_te: number
  slots_rb: number
  slots_qb: number
  slots_k: number
  slots_flex: number
  slots_def: number
  slots_bn: number
  slots_super_flex: number
  rounds: number
  pick_timer: number
  reversal_round: number
  player_type: number
  nomination_timer: number
  enforce_position_limits: number
  cpu_autopick: number
  autostart: number
  autopause_start_time: number
  autopause_end_time: number
  autopause_enabled: number
  alpha_sort: number
}

export type SleeperDraftMetadata = {
  scoring_type: string
  name: string
  description: string
}

export type SleeperDraftOrder = {
  [userId: string]: number
}

