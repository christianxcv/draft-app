import { SleeperUser } from './user'

export interface SleeperLeagueUser extends SleeperUser {
  is_owner: boolean
  metadata: SleeperLeagueUserMetadata
}

type SleeperLeagueUserMetadata = {
  team_name: string
}