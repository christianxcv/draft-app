import { GetDraftResponse } from '../../clients/models/draft';

export type QueuePlayerPayload = {
  playerId: number
}

export type DequeuePlayerPayload = {
  playerId: number
}

export type UpdateQueueOrderPayload = {
  newIndex: number
  oldIndex: number
}

export type UpdateCurrentPositionForRanksPayload = {
  position: string
}

export type ClaimDraftSlotPayload = {
  draftSlot: number
}

export type GetDraftDetailsThunkFulfilledPayload = {
  draftDetails: GetDraftResponse
}

export type DraftPlayerThunkPayload = {
  playerId: number
}

export type DeleteDraftPickFulfilledPayload = {
  playerId: number
}