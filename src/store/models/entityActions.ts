import { PlayerPosition } from '../../models/player';

export type InsertTierPayload = {
  position: PlayerPosition
  insertAfter: number
}

export type DeleteTierPayload = {
  position: PlayerPosition
  tierNumber: number
}

export type UpdatePlayerRankPayload = {
  position: PlayerPosition
  startTier: string | 'unranked'
  endTier: string | 'unranked'
  startIndex: number
  endIndex: number
  playerId: number
}

export type SaveRanksPayload = {
  position: PlayerPosition
}