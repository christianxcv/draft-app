import { createSelector } from '@reduxjs/toolkit';

import { RootState } from '../store';

const sessionStateSelector = (state: RootState) => state.session
export const loginStateSelector = createSelector(sessionStateSelector, session => session.loginState)
export const loginPageErrorMessageSelector = createSelector(sessionStateSelector, session => session.loginPageErrorMessage)