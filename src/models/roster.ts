import { DraftPick } from './draft'

export type RosterSlot = {
  slot: string
  pick?: DraftPick
}

export type Roster = {
  owner: string
  slots: RosterSlot[]
}