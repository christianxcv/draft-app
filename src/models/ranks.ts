export type RankItem = {
  key: string
  position: string
  rank: number
  tier: number
}

export type UpdateRanksRequest = {
  ranks: RankItem[]
}

export type PositionRanks = {
  tiers: { [tierId: string]: Array<number> }
  tierOrder: Array<string>
}

export type RanksByPosition = {
  [position: string]: PositionRanks
}