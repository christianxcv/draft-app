import { getPicksUntilNext } from './pickStateHelper'

describe('getPicksUtilNext', () => {

  describe('odd rounds', () => {
    it('should return 0 when userDraftSlot is 1 and currentOverall is 1', () => {
      const result  =getPicksUntilNext(1, 1, 12)
      expect(result).toEqual(0)
    })
  
    it('should return 0 when userDraftSlot is 12 and currentOverall is 12', () => {
      const result = getPicksUntilNext(12, 12, 12)
      expect(result).toEqual(0)
    })
  
    it('should return 11 when userDraftSlot is 12 and currentOverall is 1', () => {
      const result = getPicksUntilNext(12, 1, 12)
      expect(result).toEqual(11)
    })

    it('should return 3 when userDraftSlot is 5 and currentOverall is 26', () => {
      const result = getPicksUntilNext(5, 26, 12)
      expect(result).toEqual(3)
    })

    it('should return 21 when userDraftSlot is 1 and currentOverall is 26', () => {
      const result = getPicksUntilNext(1, 26, 12)
      expect(result).toEqual(21)
    })
  })
  
  describe('even rounds', () => {
    it('should return 22 when userDraftSlot is 12 and currentOverall is 14', () => {
      const result = getPicksUntilNext(12, 14, 12)
      expect(result).toEqual(22)
    })
  
    it('should return 12 when userDraftSlot is 12 and currentOverall is 24', () => {
      const result = getPicksUntilNext(12, 24, 12)
      expect(result).toEqual(12)
    })
  
    it('should return 1 when userDraftSlot is 11 and currentOverall is 13', () => {
      const result = getPicksUntilNext(11, 13, 12)
      expect(result).toEqual(1)
    })
  
    it('should return 3 when userDraftSlot is 6 and currentOverall is 16', () => {
      const result = getPicksUntilNext(6, 16, 12)
      expect(result).toEqual(3)
    })
  })

})